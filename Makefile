pythonbuild:
    python3 setup.py sdist bdist_wheel
    twine upload --repository-url https://upload.pypi.org/legacy/ dist/*

dockerbuild:
    docker build . -t katakowl/ts3ekkosingle