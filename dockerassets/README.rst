teamspeak 3 installer
=====================

The file ``TeamSpeak3-Client-linux_amd64-3.1.3.run`` is only distributed here for convenience reasons.
You should probably check out http://dl.4players.de/ts/releases/3.1.3/TeamSpeak3-Client-linux_amd64-3.1.3.run to get a download from the original source.
Or from wherever. Just make sure you name the file correctly and that you get the 3.1.3 amd64 version.

.ts3client config directory
===========================

- copied from a ts3 client 3.1.3 installation
- removed cache files
- pre-set settings for bot usage
 - no automatic gain control
 - no echo removal/prevention
 - permanent transmission