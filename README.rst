ts3ekkoclient
=============

This is the client module for ts3ekko. It is responsible for all interactions between the application and the user. It starts the ts3 client and connects to the TS3ClientQuery after initialization.

For documentation, have a look into the ``./docs/`` directory.

related projects
----------------

- https://gitlab.com/networkjanitor/vagrant-ts3ekko-deploy
- https://gitlab.com/networkjanitor/ts3ekkoutil
- https://gitlab.com/networkjanitor/ts3ekkomanage