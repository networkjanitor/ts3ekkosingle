# - alpine uses muslc (which ts3 doesn't like and compatibility is pain: frolvlad/alpine-glibc)
# - debian is huge, but sid-slim will have to do.
# - arch could work as well with elogind, but I cba to re-search all the package names, considering that sid-slim works.
FROM debian:sid-slim

# Install all the required packages
# warning: use elogind in favor of libpam-systemd (to provide the consolekit alternative systemd-logind for pulseaudio),
#          as with the later the startup times for dbus and pulseaudio are ~1 min and ~5 min respectively.
# info: consolekit2 works as well in favor of providing systemd-logind, but it's not available on debian (alpine only)
RUN apt update && apt -y install \
    alsa-utils \
    pulseaudio \
    pulseaudio-utils \
    python3.6 \
    python3-pip \
    python3-distutils \
    ca-certificates \
    elogind \
    libquazip-dev \
    libxcursor-dev \
    libxcomposite-dev \
    bzip2 \
    dbus \
    libasound2 \
    libasound2-plugins \
    libglib2.0-0 \
    libnss3 \
    libqt5core5a libqt5gui5 libqt5network5 libqt5sql5 \
    libxcursor1 \
    xvfb \
    mpv \
    libmpv1

COPY dockerassets/TeamSpeak3-Client-linux_amd64-3.2.3.run /opt/ts3install.run

# switch workdir so that TS3 installs itself into /opt/ directory
WORKDIR /opt/
RUN chmod u+x ./ts3install.run && (sleep 3 && echo "q" && sleep 3 && echo "y") | ./ts3install.run

# Fix libssl naming
RUN cd TeamSpeak3-Client-linux_amd64 && ln -s libssl.so.1.0.0 libssl_conf.so

# Copy pre-prepared ts3 configuration, needs adjustment through preparation 
# scripts regarding used identity/username
COPY dockerassets/.ts3client_3_2_3 /root/.ts3client

#RUN sed -i 's/load-module module-udev-detect/# load-module module-udev-detect/g' /etc/pulse/default.pa && \
#    echo 'load-module module-null-sink sink_name=null-teamspeak sink_properties=device.description="null-teamspeak"' >> /etc/pulse/default.pa

#ADD . /package
# Install for the preperation&start script
#RUN python3 /package/setup.py install
RUN pip3 install youtube_dl==2019.11.28 ts3ekkosingle==0.0.21.dev2

# Switch to "home" directoy for ease of use
WORKDIR /root/

ENTRYPOINT ["ts3ekkoentry"]
